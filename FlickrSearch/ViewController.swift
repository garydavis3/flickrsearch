//
//  ViewController.swift
//  FlickrSearch
//
//  Created by Gary Davis on 3/1/17.
//  Copyright © 2017 Gary Davis. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UISearchBarDelegate {
    
    let apiKey = "1dd17dde0fed7286935d83875fcc17dd"
    
    
    @IBOutlet var flickrSearch: UISearchBar!
    @IBOutlet var flickrCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let searchTerm = "Overwatch"
        
        self.flickrCollectionView.register(UINib(nibName: "FlickrCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        
        let someURL = flickrSearchURLForSearchTerm(searchTerm)
        print("here")
//        if let url = NSURL(string: searchTerm) {
//            if let data = NSData(contentsOf: someURL!) {
//                URLImage.image = UIImage(data: data)
//            }
//        }
        
        //website for UISearchController https://www.raywenderlich.com/113772/uisearchcontroller-tutorial
        
        
        let someTask = URLSession.shared.dataTask(with: someURL!) { data, response, error in
            guard error == nil else {
                print(error!)
                return
            }
            guard let data = data else {
                print("Data is empty")
                return
            }
            
            let json = try! JSONSerialization.jsonObject(with: data, options: [])
            print(json)
        }
        
        someTask.resume()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    //2
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    //3
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell",
                                                      for: indexPath) as! FlickrCollectionViewCell

        cell.someImage.backgroundColor = UIColor.orange
        cell.someLabel.backgroundColor = UIColor.blue
        //cell.someImage.image = URLImage
        cell.someLabel.text = "changed"
        // Configure the cell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width/2 - 40, height: self.view.frame.size.width/2 - 40)
    }
    
    
    fileprivate func flickrSearchURLForSearchTerm(_ searchTerm:String) -> URL? {
        
        guard let escapedTerm = searchTerm.addingPercentEncoding(withAllowedCharacters: CharacterSet.alphanumerics) else {
            return nil
        }
        
        let URLString = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=\(apiKey)&text=\(escapedTerm)&per_page=25&format=json&nojsoncallback=1"
        
        guard let someURL = URL(string:URLString) else {
            return nil
        }
        
        return someURL
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        print("\(searchBar.text)")
        
        flickrSearchURLForSearchTerm(searchBar.text!)
        
        searchBar.resignFirstResponder()
    }
    
    
}
