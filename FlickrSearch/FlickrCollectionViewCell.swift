//
//  FlickrCollectionViewCell.swift
//  FlickrSearch
//
//  Created by Gary Davis on 3/1/17.
//  Copyright © 2017 Gary Davis. All rights reserved.
//

import UIKit

class FlickrCollectionViewCell: UICollectionViewCell {

    @IBOutlet var someImage: UIImageView!
    @IBOutlet var someLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
